package com.example.demo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.example.demo.connection.ConnectionClass;
import com.exmple.demo.model.Employee;

public class EmployeeDAOImpl implements EmployeeDAO {

	Connection connection = null;

	Statement statement = null;

	
	@Override
	public List getAllEmployee() {
		List allEmployee = new ArrayList();

		try {
			connection = ConnectionClass.getConnection();
			statement = connection.createStatement();

			ResultSet rs = statement.executeQuery("select * from Employee ");
			while(rs.next())
			{
				Employee employee = new Employee();

				employee.setId(rs.getInt(1));
				employee.setName(rs.getString(2));
				employee.setAge(rs.getInt(3));
				employee.setDept(rs.getString(4));
				allEmployee.add(employee);
			}
			rs.close();
			

		} catch (Exception e) {
			e.printStackTrace();
		}
		return allEmployee;

	}

//
//	@Override
//	public Employee getEmployeeByid(int id) {
//		
//		try {
//			connection = ConnectionClass.getConnection();
//			statement = connection.createStatement();
//		
//
//			ResultSet rs = statement.executeQuery("SELECT  id , name , age , dept FROM Employee WHERE id = ? " +id);
//			while(rs.next())
//			{
//				Employee employee = new Employee();
//
//				employee.setId(rs.getInt(1));
//				employee.setName(rs.getString(2));
//				employee.setAge(rs.getInt(3));
//				employee.setDept(rs.getString(4));
//			}
//			rs.close();
//			
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//		
//
//	
//	
//	
//	}
}

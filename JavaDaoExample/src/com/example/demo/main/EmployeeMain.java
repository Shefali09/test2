package com.example.demo.main;

import java.util.List;

import com.example.demo.dao.EmployeeDAO;
import com.example.demo.dao.EmployeeDAOImpl;
import com.exmple.demo.model.Employee;

public class EmployeeMain {
	
	public static void main(String[] args)
	{
		EmployeeDAO employeedao = new EmployeeDAOImpl();
		
//		int id =100;
//		Employee e1 = employeedao.getEmployeeByid( id);
//		System.out.print(e1.toString());
		
		List<Employee> allEmployees = employeedao.getAllEmployee();
		allEmployees.forEach(emp->System.out.println(emp.toString()));
		
	}
	

}

package com.example.demo.connection;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionClass {
	
	private static final String URL ="jdbc:mariadb://127.0.0.1:3306/test";
	private static final String USR ="root";
	private static final String PASS ="root";
	private static final String driver ="org.mariadb.jdbc.Driver";
	public static Connection conn =null;
	
	public static Connection getConnection()
	{
		try {
			Class.forName(driver);
			 conn = DriverManager.getConnection(URL,USR,PASS);
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return conn;
		
	}
	
	

}

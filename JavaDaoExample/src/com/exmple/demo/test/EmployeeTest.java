package com.exmple.demo.test;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.dao.EmployeeDAOImpl;
import com.exmple.demo.model.Employee;

import junit.framework.TestCase;

public class EmployeeTest extends TestCase
{
	public void getAllEmployee1()
	{
		List<Employee> allEmployeeActual = new ArrayList<Employee>();
		EmployeeDAOImpl E1 = new EmployeeDAOImpl();
		allEmployeeActual=E1.getAllEmployee();
		
		List allEmployeeExpected = new ArrayList();
		allEmployeeExpected.add(100);
		allEmployeeExpected.add("athira");
		allEmployeeExpected.add(20000);
		allEmployeeExpected.add("software");
		
		System.out.println(allEmployeeExpected);
		System.out.println(allEmployeeActual);
		
		assertEquals(allEmployeeExpected,allEmployeeActual);
		
	}
}

package com.test;

public class RuleClass {
	
	private String TRAN;
	private String EXPLAIN;
	private String QUANTITY;
	private String MONEY;
	
	public RuleClass(String tran, String explain, String quantity, String money)
	{
		this.TRAN = tran;
		this.EXPLAIN = explain;
		this.QUANTITY = quantity;
		this.MONEY = money;
				
	}
	
	public String getMONEY() {
		return MONEY;
	}
	public void setMONEY(String mONEY) {
		MONEY = mONEY;
	}
	public String getTRAN() {
		return TRAN;
	}
	public void setTRAN(String tRAN) {
		TRAN = tRAN;
	}
	public String getEXPLAIN() {
		return EXPLAIN;
	}
	public void setEXPLAIN(String eXPLAIN) {
		EXPLAIN = eXPLAIN;
	}
	public String getQUANTITY() {
		return QUANTITY;
	}
	public void setQUANTITY(String qUANTITY) {
		QUANTITY = qUANTITY;
	}
	

}

package com.test;

public class PACE3KExcelFile {
	@Override
	public String toString() {
		return "PACE3KExcelFile [pierce=" + pierce + ", tranfi=" + tranfi + ", explain=" + explain + ", quantity="
				+ quantity + ", tcfileter=" + tcfileter + ", pifilter=" + pifilter + ", bbx=" + bbx + ", bby=" + bby
				+ ", determine=" + determine + "]";
	}
	private String pierce;
	public String getPierce() {
		return pierce;
	}
	public void setPierce(String pierce) {
		this.pierce = pierce;
	}
	public String getTranfi() {
		return tranfi;
	}
	public void setTranfi(String tranfi) {
		this.tranfi = tranfi;
	}
	public String getExplain() {
		return explain;
	}
	public void setExplain(String explain) {
		this.explain = explain;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getTcfileter() {
		return tcfileter;
	}
	public void setTcfileter(String tcfileter) {
		this.tcfileter = tcfileter;
	}
	public String getPifilter() {
		return pifilter;
	}
	public void setPifilter(String pifilter) {
		this.pifilter = pifilter;
	}
	public String getBbx() {
		return bbx;
	}
	public void setBbx(String bbx) {
		this.bbx = bbx;
	}
	public String getBby() {
		return bby;
	}
	public void setBby(String bby) {
		this.bby = bby;
	}
	public String isDetermine() {
		return determine;
	}
	public void setDetermine(String determine) {
		this.determine = determine;
	}
	private String tranfi;
	private String explain;
	private String quantity;
	private String money;
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	private String tcfileter;
	private String pifilter;
	private String bbx;
	private String bby;
	private String determine;
	public String getDetermine() {
		return determine;
	}

}

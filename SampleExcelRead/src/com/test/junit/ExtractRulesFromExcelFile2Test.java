package com.test.junit;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import com.test.ExtractRulesFromExcelFile;
import com.test.PACE3KExcelFile;
import junit.framework.TestCase;

public class ExtractRulesFromExcelFile2Test extends TestCase 
{

	@SuppressWarnings("unchecked")
	public void testGetPIRule1() throws IOException, FileNotFoundException 
	{

		ArrayList<PACE3KExcelFile> finalRule = new ArrayList<PACE3KExcelFile>();
		ExtractRulesFromExcelFile.readFromExcel("C:\\Users\\ShefaliBS\\Desktop\\P&I.xls");

		finalRule = ExtractRulesFromExcelFile.getPIRule("67", "DIV", "=0", "<0");

		Iterator<PACE3KExcelFile> finalruleItr = finalRule.iterator();
		List<String> resultValues = new ArrayList<String>();

		while (finalruleItr.hasNext()) 
		{
			PACE3KExcelFile ruleRecord = (PACE3KExcelFile) finalruleItr.next();
			String value = ruleRecord.getTcfileter() + ";" + ruleRecord.getPifilter();
			resultValues.add(value);
		}

		System.out.println("Result value 1:" + resultValues.get(0));
		System.out.println("Result value 2:" + resultValues.get(1));

		assertEquals("11;I Only", resultValues.get(0)); // pass
		assertEquals("25;I Only", resultValues.get(1)); // pass
		//assertEquals("35;I Only", resultValues.get(1)); //fail

	}

	@SuppressWarnings("unchecked")
	public void testDistinctPIRulesList() throws IOException, FileNotFoundException 
	{

		Set<String> expectedSet = new HashSet<String>();
		expectedSet.add("67!INT!=0!>0");
		expectedSet.add("67!FDV!=0!>0");
		expectedSet.add("67!FDV!=0!<0");
		expectedSet.add("67!DIV!=0!<0");
		expectedSet.add("67!DIV!=0!>0");
		expectedSet.add("67!INT!=0!<0");
		//ExpectedSet.add("76!INT!=0!<0"); 
	
		System.out.println("Expected set " + expectedSet);
		
		Set<String> actualSet = new HashSet<String>();
		actualSet = ExtractRulesFromExcelFile.addDistinctRulesFromExcel("C:\\Users\\ShefaliBS\\Desktop\\P&I.xls");

		assertEquals( expectedSet,actualSet);

	}

}

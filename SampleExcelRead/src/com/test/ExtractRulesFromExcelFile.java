package com.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Scanner;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;

public class ExtractRulesFromExcelFile 
{

	public ExtractRulesFromExcelFile() 
	{
		// default 
	}

	static List<PACE3KExcelFile> pace3kli = new ArrayList<PACE3KExcelFile>();// <PACE3KCSVFile>;
	static List<RuleClass> rcList = new ArrayList<RuleClass>();// <RuleClass>;

	public static Set<String> s = new HashSet<String>();

	public static Set addDistRules(String str) 
	{
		s.add(str);
		return s;
	}

	public static ArrayList getPIRule(String transField, String explainField, String quant, String money) 
	{
		Iterator ruleIterator = pace3kli.iterator();
		ArrayList<PACE3KExcelFile> resultantRule = new ArrayList();

		while (ruleIterator.hasNext()) 
		{
			PACE3KExcelFile ruleRecord = (PACE3KExcelFile) ruleIterator.next();
			if (ruleRecord.getTranfi().equalsIgnoreCase(transField) && ruleRecord.getQuantity().equalsIgnoreCase(quant)
					&& ruleRecord.getExplain().equalsIgnoreCase(explainField)
					&& ruleRecord.getMoney().equalsIgnoreCase(money)
					&& ruleRecord.getDetermine().equalsIgnoreCase("TRUE")) 
			{
				System.out.println();
				System.out.println("Rule Record from main prg:" + ruleRecord);

				resultantRule.add(ruleRecord);
			}
		}
		System.out.println();
		System.out.println("Resultant Rule from main prg:" + resultantRule);
		return resultantRule;

	}

	public static void main(String[] args) throws FileNotFoundException, IOException 
	{
		readFromExcel("C:\\Users\\ShefaliBS\\Desktop\\P&I.xls");
		ExtractRulesFromExcelFile obj = new ExtractRulesFromExcelFile();
		ArrayList<PACE3KExcelFile> finalRule = new ArrayList();
		
		finalRule = obj.getPIRule("67", "DIV", "=0", "<0");
		Iterator finalruleItr = finalRule.iterator();

		// System.out.println("Final rulexyz: "+finalRule);

		//1234 System.out.println("From main prg TCFileter and PIFilter data for this condition--------->>>TransField=" + 67
			//1234	+ ", ExplainField=" + "DIV" + " , Quantity=" + "=0" + ", Money=" + "<0" + " and Determine =FALSE");
		while (finalruleItr.hasNext()) {
			PACE3KExcelFile ruleRecord = (PACE3KExcelFile) finalruleItr.next();
			System.out.println("From main prg TC Filter =" + ruleRecord.getTcfileter() + ", PI Filter="
					+ ruleRecord.getPifilter());
		}

		// System.out.println("Distinct rules---->>"+rcList);
		addDistinctRulesFromExcel("C:\\Users\\ShefaliBS\\Desktop\\P&I.xls");

	}

	/**
	 * Java method to read data from Excel file in Java. This method read value from
	 * .XLS file, which is an OLE format.
	 * 
	 * @param file
	 * @throws IOException
	 */
	@SuppressWarnings("rawtypes")
	public static ArrayList readFromExcel(String file) throws IOException 
	{
		HSSFWorkbook myExcelBook = new HSSFWorkbook(new FileInputStream(file));
		// HSSFSheet myExcelSheet = myExcelBook.getSheet("PACE1");
		HSSFSheet myExcelSheet = myExcelBook.getSheetAt(0);// ("PACE");
		DataFormatter dataFormatter = new DataFormatter();
		HSSFRow row = myExcelSheet.getRow(0);

		Iterator<Row> rowIterator = myExcelSheet.iterator();
		int i = 0;
		while (rowIterator.hasNext()) 
		{

			Row row2 = rowIterator.next();
			String temp = "";

			if (i == 0) {
				i = i + 1;
				Iterator<Cell> cellIteratorheader = row.cellIterator();
				while (cellIteratorheader.hasNext()) 
				{
					Cell headercell = cellIteratorheader.next();
					String cellValue = dataFormatter.formatCellValue(headercell);
				}
			} 
			else 
			{
				// System.out.println("");

				PACE3KExcelFile excelObj = null;
				excelObj = new PACE3KExcelFile();
				excelObj.setPierce(dataFormatter.formatCellValue((row2.getCell(0))));
				excelObj.setTranfi(dataFormatter.formatCellValue((row2.getCell(1))));
				excelObj.setExplain(dataFormatter.formatCellValue((row2.getCell(2))));
				excelObj.setQuantity(dataFormatter.formatCellValue((row2.getCell(3))));
				excelObj.setMoney(dataFormatter.formatCellValue((row2.getCell(4))));
				excelObj.setTcfileter(dataFormatter.formatCellValue((row2.getCell(5))));
				excelObj.setPifilter(dataFormatter.formatCellValue((row2.getCell(6))));
				excelObj.setBbx(dataFormatter.formatCellValue((row2.getCell(7))));
				excelObj.setBby(dataFormatter.formatCellValue((row2.getCell(8))));
				excelObj.setDetermine(dataFormatter.formatCellValue((row2.getCell(9))));

				pace3kli.add(excelObj);

				temp = excelObj.getTranfi() + "!" + excelObj.getExplain() + "!" + excelObj.getQuantity() + "!"
						+ excelObj.getMoney();
				addDistRules(temp);
			} // else block ends here
		}

		myExcelBook.close();

		Iterator<String> sitr = s.iterator();
		String[] ruleStr;
		String temp = "";
		RuleClass rc = null;
		while (sitr.hasNext()) 
		{
			temp = sitr.next();
			System.out.println("Distinct rules--with ! delimeter-->>" + temp);
			ruleStr = temp.toString().split("!");
			rc = new RuleClass(ruleStr[0], ruleStr[1], ruleStr[2], ruleStr[3]);

			rcList.add(rc);
		}
		return (ArrayList) pace3kli;

	}

	public static Set addDistinctRulesFromExcel(String file) throws IOException 
	{

		Set<String> s2 = new HashSet<String>();
		HSSFWorkbook myExcelBook = new HSSFWorkbook(new FileInputStream(file));
		// HSSFSheet myExcelSheet = myExcelBook.getSheet("PACE1");
		HSSFSheet myExcelSheet = myExcelBook.getSheetAt(0);// ("PACE");
		DataFormatter dataFormatter = new DataFormatter();
		HSSFRow row = myExcelSheet.getRow(0);

		Iterator<Row> rowIterator = myExcelSheet.iterator();
		int i = 0;
		while (rowIterator.hasNext()) 
		{

			Row row2 = rowIterator.next();
			String temp = "";

			if (i == 0) 
			{
				i = i + 1;
				Iterator<Cell> cellIteratorheader = row.cellIterator();
				while (cellIteratorheader.hasNext()) 
				{
					Cell headercell = cellIteratorheader.next();
					String cellValue = dataFormatter.formatCellValue(headercell);
				}
			} 
			else 
			{
				// System.out.println("");

				PACE3KExcelFile excelObj = null;
				excelObj = new PACE3KExcelFile();
				excelObj.setPierce(dataFormatter.formatCellValue((row2.getCell(0))));
				excelObj.setTranfi(dataFormatter.formatCellValue((row2.getCell(1))));
				excelObj.setExplain(dataFormatter.formatCellValue((row2.getCell(2))));
				excelObj.setQuantity(dataFormatter.formatCellValue((row2.getCell(3))));
				excelObj.setMoney(dataFormatter.formatCellValue((row2.getCell(4))));
				excelObj.setTcfileter(dataFormatter.formatCellValue((row2.getCell(5))));
				excelObj.setPifilter(dataFormatter.formatCellValue((row2.getCell(6))));
				excelObj.setBbx(dataFormatter.formatCellValue((row2.getCell(7))));
				excelObj.setBby(dataFormatter.formatCellValue((row2.getCell(8))));
				excelObj.setDetermine(dataFormatter.formatCellValue((row2.getCell(9))));

				pace3kli.add(excelObj);

				temp = excelObj.getTranfi() + "!" + excelObj.getExplain() + "!" + excelObj.getQuantity() + "!"
						+ excelObj.getMoney();
				// addDistRules(temp);
				s2.add(temp);
			} // else block ends here
		}

		myExcelBook.close();

		Iterator<String> sitr = s.iterator();
		String[] ruleStr;
		String temp = "";
		RuleClass rc = null;
		while (sitr.hasNext()) 
		{
			temp = sitr.next();
			System.out.println("Distinct rules--with ! delimeter-->>" + temp);
			ruleStr = temp.toString().split("!");
			rc = new RuleClass(ruleStr[0], ruleStr[1], ruleStr[2], ruleStr[3]);

			rcList.add(rc);
		}
		// return (ArrayList) pace3kli;
		System.out.println("S2" + s2);
		return s2;

	}

}
